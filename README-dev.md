# ChemDataReader

ChemDataReader is a flexible python framework for reading and converting chemical data originating from webservers, including metadata and semantics.